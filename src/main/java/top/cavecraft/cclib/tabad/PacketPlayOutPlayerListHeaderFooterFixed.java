package top.cavecraft.cclib.tabad;

import net.minecraft.server.v1_8_R3.*;

import java.io.IOException;
import java.lang.reflect.Field;

import static net.minecraft.server.v1_8_R3.IChatBaseComponent.*;

public class PacketPlayOutPlayerListHeaderFooterFixed {
    private IChatBaseComponent header;
    private IChatBaseComponent footer;

    public PacketPlayOutPlayerListHeaderFooterFixed(String header, String footer) {
        this.header = ChatSerializer.a("{text: '        " + header + "'}");
        this.footer = ChatSerializer.a("{text: '        " + footer + "'}");
    }

    public PacketPlayOutPlayerListHeaderFooterFixed(IChatBaseComponent header, IChatBaseComponent footer) {
        this.header = header;
        this.footer = footer;
    }

    public PacketPlayOutPlayerListHeaderFooter getPacket() {
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter(header);

        try {
            Field footerField = packet.getClass().getDeclaredField("b");
            footerField.setAccessible(true);
            footerField.set(packet, footer);
            footerField.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();System.out.println(e.getMessage());
        }

        return packet;
    }
}