package top.cavecraft.cclib.chat;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;

import java.util.HashMap;
import java.util.Map;

public class ColorConverter {
    ChatColor color;
    Map<ChatColor, DyeColor> colorMap = new HashMap<>();

    ColorConverter(ChatColor color) {
        this.color = color;
        colorMap.put(ChatColor.RED, DyeColor.RED);
        colorMap.put(ChatColor.BLUE, DyeColor.BLUE);
        colorMap.put(ChatColor.GREEN, DyeColor.LIME);
        colorMap.put(ChatColor.YELLOW, DyeColor.YELLOW);
    }

    public DyeColor getWoolColor() {
        return colorMap.getOrDefault(color, DyeColor.BLACK);
    }
}
