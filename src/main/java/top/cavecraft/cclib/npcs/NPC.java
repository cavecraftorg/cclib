package top.cavecraft.cclib.npcs;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.UUID;
import java.util.regex.Pattern;

import static top.cavecraft.cclib.CCLib.plugin;

public class NPC {
    final Location location;
    private final String name;
    private String displayName;
    private String prefix;
    private String suffix;
    private EntityPlayer entityPlayer;
    private final String textureCache;
    private final String signatureCache;
    private final UUID uuid;
    private static final Pattern NON_ALPHABET_MATCHER = Pattern.compile(".*[^A-Za-z0-9_].*");

    NPC(Location location, String name, String texture, String signature) {
        this.name = name;
        this.location = location;
        this.textureCache = texture;
        this.signatureCache = signature;
        this.uuid = UUID.randomUUID();
    }

    public NPC addToWorld() {
        try {
            //Big block of code that gets the skin information from the mojang servers.
            /*URL url = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + skinUUID + "?unsigned=false");
            URLConnection connection = url.openConnection();
            InputStream inputStream = connection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder builder = new StringBuilder();
            bufferedReader.lines().forEach(builder::append);

            JsonObject profile = new Gson().fromJson(builder.toString(), JsonObject.class);
            JsonObject textureProperty = profile.get("properties").getAsJsonArray().get(0).getAsJsonObject();
            String textureCache = textureProperty.get("value").getAsString();
            String signatureCache = textureProperty.get("signature").getAsString();*/

            MinecraftServer minecraftServer = ((CraftServer) Bukkit.getServer()).getServer();
            WorldServer worldServer = ((CraftWorld) location.getWorld()).getHandle();

            if (name.length() > 16) {
                prefix = name.substring(0, 16);
                if (name.length() > 30) {
                    int len = 30;
                    displayName = name.substring(16, 30);
                    if (NON_ALPHABET_MATCHER.matcher(displayName).matches()) {
                        if (name.length() >= 32) {
                            len = 32;
                            displayName = name.substring(16, 32);
                        } else {
                            len = 31;
                            displayName = name.substring(16, 31);
                        }
                    } else {
                        displayName = ChatColor.RESET + displayName;
                    }
                    suffix = name.substring(len);
                } else {
                    displayName = name.substring(16);
                    if (!NON_ALPHABET_MATCHER.matcher(displayName).matches()) {
                        displayName = ChatColor.RESET + displayName;
                    }
                    if (displayName.length() > 16) {
                        suffix = displayName.substring(16);
                        displayName = displayName.substring(0, 16);
                    }
                }
            } else {
                displayName = name;
            }

            GameProfile gameProfile = new GameProfile(uuid, displayName);
            gameProfile.getProperties().put("textures", new Property("textures", textureCache, signatureCache));

            entityPlayer = new EntityPlayer(minecraftServer, worldServer, gameProfile, new PlayerInteractManager(worldServer));
            entityPlayer.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());

            entityPlayer.getBukkitEntity().setSleepingIgnored(true);
        } catch (Exception e) {
            e.printStackTrace();System.out.println(e.getMessage());
        }

        return this;
    }

    public void sendToPlayer(Player player) {
        if (!player.isValid() || !player.isOnline()) return;
        try {
            PlayerConnection playerConnection = ((CraftPlayer) player).getHandle().playerConnection;

            playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer));
            playerConnection.sendPacket(new PacketPlayOutNamedEntitySpawn(entityPlayer));
            playerConnection.sendPacket(new PacketPlayOutEntityHeadRotation(entityPlayer, (byte) (entityPlayer.yaw * 256 / 360)));

            //Enables multiple skin layers, stolen from https://www.spigotmc.org/threads/npc-skin-second-layer-doesnt-showing-up.277869/
            DataWatcher watcher = entityPlayer.getDataWatcher();
            watcher.watch(10, (byte) 127);
            playerConnection.sendPacket(new PacketPlayOutEntityMetadata(entityPlayer.getId(), watcher, true));

            plugin.getServer().getScheduler().runTaskLater(plugin, () -> hide(player), 100);

            plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
                Team team = plugin.scoreboardManager.getScoreboardFor(player).getHandle().registerNewTeam(displayName);
                if (prefix != null) {
                    team.setPrefix(prefix);
                }
                if (suffix != null) {
                    team.setSuffix(suffix);
                }
                team.addEntry(entityPlayer.getBukkitEntity().getName());
            }, 1);
        } catch (Exception e) {
            e.printStackTrace();System.out.println(e.getMessage());
        }
    }

    public void hide(Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer));
    }

    public UUID getUuid() {
        return uuid;
    }
}
