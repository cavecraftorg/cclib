package top.cavecraft.cclib.npcs;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.List;

import static top.cavecraft.cclib.CCLib.*;

public class NPCManager implements Listener {
    List<NPC> npcList = new ArrayList<>();
    List<ServerNPC> serverNPCList = new ArrayList<>();

    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {
        for (NPC npc : npcList) {
            npc.sendToPlayer(event.getPlayer());
        }

        for (ServerNPC npc : serverNPCList) {
            npc.sendToPlayer(event.getPlayer());
        }
    }

    @EventHandler
    public void playerInteract(PlayerInteractEvent event) {
        for (ServerNPC npc : serverNPCList) {
            npc.handleClick(event);
        }
    }

    public void addNPC(Location location, String name, String texture, String signature) {
        NPC npc = new NPC(location, name, texture, signature);
        npcList.add(npc);
        npc.addToWorld();
    }

    public void addServerNPC(Location location, String name, String texture, String signature, String server) {
        ServerNPC npc = new ServerNPC(location, name, texture, signature, server);
        serverNPCList.add(npc);
        npc.addToWorld();
    }
}
