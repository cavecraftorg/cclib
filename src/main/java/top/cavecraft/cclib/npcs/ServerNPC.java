package top.cavecraft.cclib.npcs;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import static top.cavecraft.cclib.CCLib.plugin;

public class ServerNPC extends NPC {
    String server;

    ServerNPC(Location location, String displayName, String texture, String signature, String server) {
        super(location, displayName, texture, signature);
        this.server = server;
    }

    //TODO: deprecated so I get a warning, make this actually calculate if the player is looking at the NPC
    @Deprecated
    public void handleClick(PlayerInteractEvent event) {
        if (event.getPlayer().getLocation().distance(super.location) <= 2) {
            sendToServer(event.getPlayer());
        }
    }

    public void sendToServer(Player player) {
        plugin.sendToServer(player, server);
    }
}
