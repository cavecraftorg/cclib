package top.cavecraft.cclib;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.*;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import top.cavecraft.cclib.chat.ActionBarMessage;
import top.cavecraft.cclib.game.GameUtils;
import top.cavecraft.cclib.game.TickReq;
import top.cavecraft.cclib.mongodriverdriver.CollectionDriver;
import top.cavecraft.cclib.mongodriverdriver.DatabaseDriver;
import top.cavecraft.cclib.npcs.NPCManager;
import top.cavecraft.cclib.scoreboard.ChildScoreboard;
import top.cavecraft.cclib.scoreboard.ScoreboardManager;
import top.cavecraft.cclib.skynet.SkyNetAC;
import top.cavecraft.cclib.tabad.PacketPlayOutPlayerListHeaderFooterFixed;

import java.util.*;

public final class CCLib extends JavaPlugin implements ICCLib, Listener {
    public static CCLib plugin;

    public GameUtils gameUtils = null;
    public ScoreboardManager scoreboardManager;
    public NPCManager npcManager;
    public SkyNetAC skynet;

    public static String uuid = "";

    //I could localize these. French support, anyone?
    private static final String gameSelectorText = "" + ChatColor.RESET + ChatColor.GREEN + "Game Selector " + ChatColor.GRAY + "[RCLICK]";
    private static final String closeMenuText = "" + ChatColor.RESET + ChatColor.RED + "Close Menu";
    private static final String squidWarsText = "" + ChatColor.RESET + ChatColor.AQUA + "Squid Wars";
    private static final String spleefRunText = "" + ChatColor.RESET + ChatColor.AQUA + "Spleef Run";
    private static final String survivalText = "" + ChatColor.RESET + ChatColor.AQUA + "Survival";

    private DatabaseDriver database;
    public CollectionDriver servers;
    public CollectionDriver players;
    public CollectionDriver analytics;

    IChatBaseComponent clickableAd = ChatSerializer.a("[{\"text\":\"Click -> \",\"bold\":true,\"color\":\"yellow\"},{\"text\":\"https://\",\"bold\":true,\"color\":\"dark_green\",\"clickEvent\":{\"action\":\"open_url\",\"value\":\"https://cavecraft.top/shop?ref=ingame\"},\"hoverEvent\":{\"action\":\"show_text\",\"contents\":{\"text\":\"Click to open URL\"}}},{\"text\":\"cavecraft.top\",\"color\":\"white\",\"clickEvent\":{\"action\":\"open_url\",\"value\":\"https://cavecraft.top/shop?ref=ingame\"},\"hoverEvent\":{\"action\":\"show_text\",\"contents\":{\"text\":\"Click to open URL\"}}},{\"text\":\"/shop/\",\"color\":\"gray\",\"clickEvent\":{\"action\":\"open_url\",\"value\":\"https://cavecraft.top/shop?ref=ingame\"},\"hoverEvent\":{\"action\":\"show_text\",\"contents\":{\"text\":\"Click to open URL\"}}}]");
    IChatBaseComponent unClickableAd = ChatSerializer.a("[{\"text\":\"Buy cavegems at \",\"bold\":true,\"color\":\"gold\"},{\"text\":\"https://\",\"bold\":true,\"color\":\"dark_green\"},{\"text\":\"cavecraft.top\",\"color\":\"white\"},{\"text\":\"/shop/\",\"color\":\"gray\"}]");

    @Override
    public void onEnable() {
        plugin = this;

        getServer().getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");

        if (getConfig().getString("uuid") == null) {
            getConfig().addDefault("uuid", UUID.randomUUID().toString());
            getConfig().options().copyDefaults(true);
            saveDefaultConfig();
            saveConfig();
        }

        uuid = getConfig().getString("uuid");

        database = new DatabaseDriver("mongodb://localhost:27017", "test");

        servers = database.getCollection("servers");
        players = database.getCollection("players");
        analytics = database.getCollection("analytics");

        servers.createOrReplace(
                uuid,
                new Document()
                        .append("address", getServer().getIp())
                        .append("port", getServer().getPort())
                        .append("gameType", getConfig().getString("gametype"))
                        .append("players", 0)
                        .append("available", true)
        );

        scoreboardManager = new ScoreboardManager();
        npcManager = new NPCManager();
        skynet = new SkyNetAC();
        getServer().getPluginManager().registerEvents(skynet, this);
        getServer().getPluginManager().registerEvents(npcManager, this);
        getServer().getPluginManager().registerEvents(scoreboardManager, this);
        getServer().getPluginManager().registerEvents(this, this);

        if (getConfig().getString("gametype").equals("lobby")) {
            npcManager.addServerNPC(
                    new Location(Bukkit.getWorlds().get(0), -16.5, 63.0, -3.5, -90, 0),
                    squidWarsText,
                    "ewogICJ0aW1lc3RhbXAiIDogMTYwNjU5MDgwNDQ5NywKICAicHJvZmlsZUlkIiA6ICIyMjVjOTA2ZjlkMjM0MjE2YjE0NGI3NzkyOWY5YjRmNyIsCiAgInByb2ZpbGVOYW1lIiA6ICJEb2N0b3JSb2JvY3RvciIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8xM2VhNjBlYjBjYmNlNmFhOTk4MjcyYzgwZjU5ZWYzNTkyM2Y4ODc5YmI4ZDBmNzM4ZmQ2NTc1ZGE5NGMzMWE2IgogICAgfQogIH0KfQ==",
                    "WnrB6WLg8Ki5S5fFrvZm9oVMXtalSnNsYJBeoPGuig4vE22lKl4KhQb7/hGhjlGvolV2we5I5KgiCR85QwAj3pZu97FJM3N7qqV42clWKq1+RjKki5BLhP0Db+5nuOnog3s8kUTPH7GI3i/nFy+xtfv0EgsLBeuAZFdDsygnEmB47WaI+sICIuHLCTQmcSsk41sRmI1F9Npmg+MNTTHnji4ugmBzYN0I+CpeAurx9HU4KUf8YbNc7kRIOJoc+oy7bJW0bJ50OWc8A+7uH2234Fc/p25iNwAZFbQhj/DdnTLfL/NlonZZ4SZNY73/Xik40fIhYPFfi5eqsEkKQQJmyMjjol+M2wTpYRQqxyYCfXz85tZCU/RVyF1+7n/4aCTXsoX6y3H1LrWDtH8pirQ1yCd9oDhVnPQ4O21pQA05eRmEd4aoTrJMYX6Bt18v+NkDWWnt2Eq/Lr87VYO9yIW3HXzr91uYdVs/vEIKn1vgEI9XmX6bGkfn+VPm+lSkc90PPXlLDB5BwpscM7gCP32EXKjhBNf0muesclo0NAg4ssJeNbeGJ6ZWN1ANhszA7SNN07HvolTHdMd6xD7ZCfsh6glQxLw/qLiCj3qjk7CVwOhcyNC/xUJPMddch4cEASXzp3bpkZgvou3SufuBp8d2WK5v/Si6iEBs4hiBbUtBo4Q=",
                    "squidwars"
            );

            npcManager.addServerNPC(
                    new Location(Bukkit.getWorlds().get(0), -17.5, 63.0, 0.5, -90, 0),
                    spleefRunText,
                    "ewogICJ0aW1lc3RhbXAiIDogMTYwNjU5MTEzODQ2MiwKICAicHJvZmlsZUlkIiA6ICIyMjVjOTA2ZjlkMjM0MjE2YjE0NGI3NzkyOWY5YjRmNyIsCiAgInByb2ZpbGVOYW1lIiA6ICJEb2N0b3JSb2JvY3RvciIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8xZWQ2MzRiMGI0OWVlMGZkZjRkMWQ2ODkzZTg2YzgwM2RkNjRkMmZiNTQ1MTU2MTExMmNlMjk2OTY5N2ZhMGEiCiAgICB9CiAgfQp9",
                    "ydkz4tDzcbny+Dbiacr3EULzQePv+i+1NGhIADia0Q4x6ENBzG1zs1uQzh1AlGZfUx7pBackaBOMkM00YQGFo8rJ33F1m8judKb1g2B2+feeJXN9C2Hc16pmMBhHCrQRH8qnnzK04FSjp91ANnsHfV5xfMoFcV8QYutrJ2bYhTnTZSf51Qh+MRsO8JaaqdZ0apDejM2KPwREPMy2nFTWt7AJFeM1hQeJ6QWfo0gnB08gP7eiJUrEQho8t3O+Nvw1z8uVbVNG/eYeFrMxlZNBRqus893MF4yuQtyBScYchvCy/lZjqUc/FUWK3hAjCyaSdnptT5/Nsq35fnjskGvJEd1RC/YAObsgrlv0hISF1cQiSUldc65Bm+eUqDaA3tSa7icYxaSsAITEqTxLlNrskz1+iL5ayi+LLCJhZVOM1igk8kAe0fCpZxI2B4MQ1bijTlA48mqHcBW2qLTP3UweCfR3KXwN1T0V+EHhwCtH9XoLOJ1WOCpVUbOahq/P6jPt0E31WtYWjblhjB83kByW79luGtfe2twn524s1mR21cO44eHYmT+OBxJgV5TM27hx5A8wy9ToD6aDVtzAuESH1B8F8SPF4RbtU8dIP0JkV66pHN0qKnY1mpkHoxQCwKIJ3AyEks8nfG4FpAbNjxTWGVykaPC2cdNL5mww0cPghLY=",
                    "spleefrun"
            );

            npcManager.addServerNPC(
                    new Location(Bukkit.getWorlds().get(0), -16.5, 63.0, 4.5, -90, 0),
                    survivalText,
                    "ewogICJ0aW1lc3RhbXAiIDogMTYwODk2MzE3MTkzNCwKICAicHJvZmlsZUlkIiA6ICIyMjVjOTA2ZjlkMjM0MjE2YjE0NGI3NzkyOWY5YjRmNyIsCiAgInByb2ZpbGVOYW1lIiA6ICJEb2N0b3JSb2JvY3RvciIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8zYWIyOTQxMjliZjkzNDMxYzk2NmZlOGE4OTcyZTA0NGU2MzM4ZmVjMzM3MDQ3YjJlN2EzNjc5NDE1M2MyZjFhIgogICAgfQogIH0KfQ==",
                    "c+ht7/uXkBNOT4uDlsIIrt4D5ZxMZlE+9kptUz9HgmZK9paii4+22FiS02UpUGLR7+XAZvRyr92xgeN4xv0jVch5DFKPZ4alneajgcLpqUQJAQ0q/W8fzid5+YryDFWUySWAVOeuu4UBO/b2QvNAtvytNwq2M3Gt0posnnI4z6q+2oJnW1C9kdzYvsXqIR7zTjSrtstYYRcjAe13NROGFy/rFcaZUr7rIW/7Z6EaejrsgYnX0UEeusluLmrzW+0KEaMelj7lX0//JxlWqlRgy3RTXBh6ElLHDGwz2GGWebGzRSd3KOeHhzjscGHtKiSzmOpjPpNmYlRhNAyjy4GtwuXkfA/tf4IQvQaNEgNcObnkgs2qF3r6fEiGRvRFkvL5Qc+JC9HPwXwec4p4adfSF7nqUlWPOqQTBedLtaNGfFyvb/HoH0UfFxLceFC+WLWO3yoivI+7K1MNxZWOqONufKkGAT65NEJgHaeTD0qlQbFcMV2TKlNNYguUfkfG3SHb8s42cpN3stGb3wlNKgYoHi+CSQgFXyYHgRwVZo+qWKQyRK+yDHEkfdSwM4UtSjxcj13dBxPabEnoLB7mtfEVlnuf9EoKi1Ja8E4UfB+zhXlz4icMHmebQIcMWncNX9Q5K7VkZeIvHV2iHwWSEjQBLpduTwtmgq28h+VauqDGA/8=",
                    "survival"
            );
        }

        getServer().getScheduler().runTaskTimer(this, this::sendAd, 12000, 12000);

        getServer().getScheduler().runTaskTimer(this, () -> {
            for (Slime slime : Bukkit.getWorlds().get(0).getEntitiesByClass(Slime.class)) {
                if (slime.getCustomName().equals(ChatColor.DARK_PURPLE + "Totally an Astrageldon Slime")) {
                    Bukkit.getWorlds().get(0).playEffect(slime.getLocation(), Effect.FOOTSTEP,0, 1);
                }
            }
        }, 40, 40);
    }

    @Override
    public void onDisable() {
        setAvailable(false);
    }

    @Override
    public IGameUtils enableGameFeatures(List<ITickReq> tickRequirements) {
        gameUtils = new GameUtils(tickRequirements);
        return gameUtils;
    }

    @Override
    public void enableGameFeatures(List<ITickReq> tickRequirements, Runnable startGame, Runnable teleport) {
        gameUtils = new GameUtils(tickRequirements, startGame, teleport);
        getServer().getPluginManager().registerEvents(gameUtils, this);
    }

    @Override
    public IGameUtils getGameUtils() {
        return gameUtils;
    }

    @Override
    public ITickReq createTickReq(int numberOfPlayers, int seconds, boolean canStart) {
        return new TickReq(numberOfPlayers, seconds, canStart);
    }

    @Override
    public ItemStack createNamedItem(Material material, int amount, String name) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        item.setItemMeta(itemMeta);
        return item;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("update") && sender.hasPermission("cclib.admin")) {
            restartServer();
            return true;
        }
        return false;
    }

    @Override
    public void restartServer() {
        setAvailable(false);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            List<String> command = new ArrayList<>();
            command.add("./restart.sh");
            try {
                new ProcessBuilder(command).start();
            }
            catch(Exception e) {
                e.printStackTrace();System.out.println(e.getMessage());
            }
        }));

        Bukkit.shutdown();
    }

    @Override
    public IScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }

    @Override
    @Deprecated
    public void awardGems(Player player, IGameUtils.GemAmount amount) {/*
        try {
            double total;
            int intAmount = (int) (GemAmountExtractor.getAmount(amount) * 10.0);

            Document playerDocument = gems.find(eq("uuid", player.getUniqueId().toString())).first();

            if (playerDocument == null || playerDocument.get("cgems") == null) {
                playerDocument = new Document("_id", new ObjectId());
                playerDocument
                        .append("uuid", player.getUniqueId().toString())
                        .append("cgems", intAmount);
                gems.insertOne(playerDocument);
            }

            try {
                total = playerDocument.getInteger("cgems") + intAmount;
            } catch (ClassCastException e) {
                //This converts old playerdata to integers.
                total = Math.round(playerDocument.getDouble("cgems")) + intAmount;
            }

            gems.updateOne(
                    eq("uuid", player.getUniqueId().toString()),
                    set("cgems", total)
            );

            player.sendMessage(
                    ChatColor.DARK_PURPLE +
                            "You earned " +
                            ChatColor.GOLD +
                            (intAmount / 10.0) +
                            ChatColor.DARK_PURPLE +
                            " CaveGems. Total: " +
                            ChatColor.GOLD +
                            (total / 10.0)
            );
            player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 2);
        } catch (Exception e) {
            player.sendMessage(ChatColor.RED + "Error occured while awarding gems.");
            e.printStackTrace();System.out.println(e.getMessage());
        }*/
    }

    @Override
    @Deprecated
    public double getGems(Player player) {/*
        try {
            double total;

            Document playerDocument = gems.find(eq("uuid", player.getUniqueId().toString())).first();

            if (playerDocument == null || playerDocument.get("cgems") == null) {
                playerDocument = new Document("_id", new ObjectId());
                playerDocument
                        .append("uuid", player.getUniqueId().toString())
                        .append("cgems", 0);
                gems.insertOne(playerDocument);
            }

            try {
                total = playerDocument.getInteger("cgems");
            } catch (ClassCastException e) {
                //This converts old playerdata to integers.
                total = Math.round(playerDocument.getDouble("cgems"));
            }

            return total / 10.0;
        } catch (Exception e) {
            e.printStackTrace();System.out.println(e.getMessage());
            return 0;
        }*/
        return 0;
    }

    @Override
    @Deprecated
    public void decreaseGems(Player player, double decrease) {/*
        try {
            double total;
            int intAmount = (int) Math.round(decrease * -10.0);

            Document playerDocument = gems.find(eq("uuid", player.getUniqueId().toString())).first();

            if (playerDocument == null || playerDocument.get("cgems") == null) {
                playerDocument = new Document("_id", new ObjectId());
                playerDocument
                        .append("uuid", player.getUniqueId().toString())
                        .append("cgems", intAmount);
                gems.insertOne(playerDocument);
            }

            try {
                total = playerDocument.getInteger("cgems") + intAmount;
            } catch (ClassCastException e) {
                //This converts old playerdata to integers.
                total = Math.round(playerDocument.getDouble("cgems")) + intAmount;
            }

            gems.updateOne(
                    eq("uuid", player.getUniqueId().toString()),
                    set("cgems", total)
            );

            player.sendMessage(
                    ChatColor.DARK_PURPLE +
                            "You earned " +
                            ChatColor.GOLD +
                            (intAmount / 10.0) +
                            ChatColor.DARK_PURPLE +
                            " CaveGems.Total: " +
                            ChatColor.GOLD +
                            (total / 10.0)
            );
            player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 2);
        } catch (Exception e) {
            player.sendMessage(ChatColor.RED + "Error occured while awarding gems.");
            e.printStackTrace();System.out.println(e.getMessage());
        }*/
    }

    public void setAvailable(boolean available) {
        servers.get(uuid).setValue("available", available);
    }

    public void setPlayers(int players) {
        servers.get(uuid).setValue("players", players);
    }

    @Override
    public IChildScoreboard createChildScoreboard() {
        return new ChildScoreboard();
    }

    @Override
    public Location offset(Location original, double xoff, double yoff, double zoff) {
        return new Location(original.getWorld(), original.getX() + xoff, original.getY() + yoff, original.getZ() + zoff);
    }

    @Override
    public IActionBarMessage createActionBarMessage(String message) {
        return new ActionBarMessage(message);
    }

    @EventHandler
    public void achievement(PlayerAchievementAwardedEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void connect(PlayerJoinEvent event) {
        try {
            if (getServer().getOnlinePlayers().size() >= getServer().getMaxPlayers() && gameUtils == null) {
                setAvailable(false);
            }
            setPlayers(getServer().getOnlinePlayers().size());

            PlayerConnection connection = ((CraftPlayer) event.getPlayer()).getHandle().playerConnection;
            connection.sendPacket(new PacketPlayOutPlayerListHeaderFooterFixed(
                    ChatSerializer.a("[{\"text\":\"You are playing on \",\"bold\":true,\"color\":\"gold\"},{\"text\":\"cavecraft.top\",\"bold\":true,\"color\":\"green\"}]"),
                    unClickableAd
            ).getPacket());

            Map<Statistic, Integer> achievements = new HashMap<>();
            achievements.put(AchievementList.f, 1);
            connection.sendPacket(new PacketPlayOutStatistic(achievements));
        } catch (Exception e) {
            e.printStackTrace();System.out.println(e.getMessage());System.out.println(e.getMessage());
        }
    }
    
    public void sendAd() {
        for (Player player : getServer().getOnlinePlayers()) {
            //Make this more targeted per player.
            player.sendMessage("");
            player.sendMessage("            [" + ChatColor.GREEN + "ADVERTISEMENT" + ChatColor.WHITE + "]");
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(
                    clickableAd
            ));
            player.sendMessage(ChatColor.YELLOW + "Every purchase" + ChatColor.BOLD + " makes the server better!");
            player.sendMessage("");
        }
    }

    @EventHandler
    public void leave(PlayerQuitEvent event) {
        if (getServer().getOnlinePlayers().size() < getServer().getMaxPlayers() && gameUtils == null) {
            setAvailable(true);
        }
        setPlayers(getServer().getOnlinePlayers().size());
    }

    @EventHandler
    public void kick(PlayerKickEvent event) {
        if (getServer().getOnlinePlayers().size() < getServer().getMaxPlayers() && gameUtils == null) {
            setAvailable(true);
        }
        setPlayers(getServer().getOnlinePlayers().size());
    }

    public void sendToServer(Player player, String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();

        out.writeUTF("Connect");
        out.writeUTF(server);

        player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
    }
}
