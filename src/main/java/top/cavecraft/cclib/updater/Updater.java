package top.cavecraft.cclib.updater;

import org.bukkit.ChatColor;

import java.util.*;
import java.util.regex.Pattern;

import static top.cavecraft.cclib.CCLib.plugin;

public class Updater {
    List<String> downloadLinks = Arrays.asList(
            "https://www.dropbox.com/s/nfhmlqvprf27zts/CCLib-STABLE.jar?dl=1",
            "https://www.dropbox.com/s/gffvvqinf9mjhs6/LuckPerms.jar?dl=1",
            "https://www.dropbox.com/s/y148rfxc87rwf89/Vault.jar?dl=1"
    );

    public boolean checkForUpdates() {
        return false;
    }

    public void addCommands(List<String> listToAddTo) {
        plugin.getServer().broadcastMessage(ChatColor.RED + "Updating server.");

        listToAddTo.add("rm -rf ./CCLib-*.jar");
        listToAddTo.add("rm -rf ./LuckPerms-*.jar");
        listToAddTo.add("rm -rf ./Vault.jar");

        for (String link : downloadLinks) {
            listToAddTo.add("wget " + link + " --output-document " + link.split("\\?")[0].split("/")[5]);
        }
    }
}
