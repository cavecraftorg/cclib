package top.cavecraft.cclib.game;

import top.cavecraft.cclib.ICCLib;

public class TickReq implements ICCLib.ITickReq {
    private int numberOfPlayers; //Players necessary to shorten to the number of ticks
    private int numberOfTicks;
    private boolean gameCanStart;

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public int getNumberOfTicks() {
        return numberOfTicks;
    }

    public boolean getIfGameCanStart() {
        return gameCanStart;
    }

    public TickReq(int numberOfPlayers, int seconds, boolean gameCanStart) {
        this.numberOfPlayers = numberOfPlayers;
        this.numberOfTicks = seconds * 20;
        this.gameCanStart = gameCanStart;
    }
}
