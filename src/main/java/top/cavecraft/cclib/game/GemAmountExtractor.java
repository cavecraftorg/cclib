package top.cavecraft.cclib.game;

import top.cavecraft.cclib.ICCLib;

public class GemAmountExtractor {
    public static double getAmount(ICCLib.IGameUtils.GemAmount amount) {
        switch (amount) {
            case SMALL:
                return 0.1;
            case MEDIUM:
                return 0.3;
            case LARGE:
                return 0.5;
            case EXTRA_LARGE:
                return 0.7;
        }
        return 0;
    }
}
