package top.cavecraft.cclib.mongodriverdriver;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class DatabaseDriver {
    private final MongoDatabase mongo;
    private final MongoClient mongoClient;

    public DatabaseDriver(String uri, String name) {
        ConnectionString connectionString = new ConnectionString(uri);
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .retryWrites(true)
                .build();
        mongoClient = MongoClients.create(mongoClientSettings);
        mongo = mongoClient.getDatabase(name);
        Runtime.getRuntime().addShutdownHook(new Thread(mongoClient::close));
    }

    public MongoDatabase getMongo() {
        return mongo;
    }

    public CollectionDriver getCollection(String name) {
        return new CollectionDriver(getMongo().getCollection(name));
    }
}
