package top.cavecraft.cclib.mongodriverdriver;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;

public class CollectionDriver {
    private final MongoCollection<Document> collection;

    public CollectionDriver(MongoCollection<Document> collection) {
        this.collection = collection;
    }

    public DocumentDriver get(String uuid) {
        return getOrDefault(uuid, new Document().append("uuid", uuid));
    }

    public DocumentDriver get(String keyName, String key) {
        return getOrDefault(keyName, key, new Document().append(keyName, key));
    }

    public DocumentDriver getOrDefault(String uuid, Document defaultDocument) {
        return getOrDefault("uuid", uuid, defaultDocument);
    }

    public boolean has(String uuid) {
        return collection.find(eq("uuid", uuid)).first() != null;
    }

    public boolean has(String key, String keyName) {
        return collection.find(eq(keyName, key)).first() != null;
    }

    public DocumentDriver getOrDefault(String keyName, String key, Document defaultDocument) {
        Document document = collection.find(eq(keyName, key)).first();

        if (document != null) {
            return new DocumentDriver(this, document.getString("uuid"));
        } else {
            return createOrReplace(keyName, key, defaultDocument);
        }
    }

    public DocumentDriver add(Document defaultDocument) {
        return createOrReplace("uuid", UUID.randomUUID().toString(), defaultDocument);
    }

    public DocumentDriver createOrReplace(String uuid, Document defaultDocument) {
        return createOrReplace("uuid", uuid, defaultDocument);
    }

    public DocumentDriver createOrReplace(String keyName, String key, Document defaultDocument) {
        try {
            getCollection().deleteMany(eq(keyName, key));
            defaultDocument.put(keyName, key);
            getCollection().insertOne(defaultDocument);
            return new DocumentDriver(this, key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public MongoCollection<Document> getCollection() {
        return collection;
    }
}
