package top.cavecraft.cclib.mongodriverdriver;

import org.apache.commons.lang.NullArgumentException;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;

public class DocumentDriver {
    private final CollectionDriver collection;
    private final String uuid;

    public DocumentDriver(CollectionDriver collection, String uuid) {
        this.collection = collection;
        this.uuid = uuid;
    }

    public DocumentDriver setValue(String key, Object value) {
        collection.getCollection().updateOne(
                eq("uuid", uuid),
                set(key, value)
        );
        return this;
    }

    /**
     *
     *
     * @param key
     * The key associated with the value in the document
     *
     * @return
     * The value
     *
     * @throws NullPointerException
     * If there is no document in the database that matches it's uuid. This usually happens if you instantiate this on
     * your own rather than using {@link CollectionDriver#get(String)} or you use that function and you haven't created
     * the document in the first place either by using {@link CollectionDriver#getOrDefault(String, Document)} or using
     *
     */
    public Object getValue(String key) {
        Document document = collection.getCollection().find(
            eq("uuid", uuid)
        ).first();
        if (document != null) {
            return document.get(key);
        } else {
            throw new NullArgumentException("A DocumentDriver was created without a Mongo document matching it's UUID!");
        }
    }

    public Object getValueOrDefault(String key, Object defaultValue) {
        Object value = getValue(key);

        if (value != null) {
            return value;
        } else {
            return defaultValue;
        }
    }

    public void deleteValue(String key) {
        collection.getCollection().updateOne(
                eq("uuid", uuid),
                unset(key)
        );
    }

    public String getUUID() {
        return uuid;
    }
}
