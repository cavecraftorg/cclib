package top.cavecraft.cclib.skynet.movement;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffectType;
import top.cavecraft.cclib.skynet.Flag;

public class SpeedCheck extends MovementCheck {
    long lastMillis = 0;

    @Override
    public Flag runMovementCheck(PlayerMoveEvent event) {
        if (event.getPlayer().hasPotionEffect(PotionEffectType.SPEED) || event.getPlayer().isFlying()) {
            return new Flag();
        }

        Location fromFlat = event.getFrom().clone();
        fromFlat.setY(0);
        Location toFlat = event.getTo().clone();
        toFlat.setY(0);

        if (fromFlat.distanceSquared(toFlat) >= 1.2) {
            return new Flag(100, "speed distance per event check");
        }

        return new Flag();
    }

    @Override
    public void runTick(Player suspect) {

    }
}
