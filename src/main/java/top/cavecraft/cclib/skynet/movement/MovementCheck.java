package top.cavecraft.cclib.skynet.movement;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerMoveEvent;
import top.cavecraft.cclib.skynet.Check;
import top.cavecraft.cclib.skynet.Flag;

public abstract class MovementCheck extends Check {
    public MovementCheck() {
        super(PlayerMoveEvent.class);
    }

    @Override
    public Flag runAbstractCheck(Player suspect, Event event) {
        return runMovementCheck((PlayerMoveEvent) event);
    }

    public abstract Flag runMovementCheck(PlayerMoveEvent event);
}
