package top.cavecraft.cclib.skynet.movement;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import top.cavecraft.cclib.skynet.Flag;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FlyCheck extends MovementCheck {
    Map<UUID, Integer> upwardMovement = new HashMap<>();
    Map<UUID, Double> prevVelChange = new HashMap<>();

    @Override
    public Flag runMovementCheck(PlayerMoveEvent event) {
        if (event.getPlayer().isFlying()) {
            return new Flag();
        }

        Location fromFlat = event.getFrom().clone();
        fromFlat.setY(0);
        Location toFlat = event.getTo().clone();
        toFlat.setY(0);

        if (upwardMovement.containsKey(event.getPlayer().getUniqueId())) {
            if (!onGround(event) && event.getTo().getY() >= event.getFrom().getY() - 0.01) {
                upwardMovement.put(event.getPlayer().getUniqueId(), upwardMovement.get(event.getPlayer().getUniqueId()) + 1);
            } else {
                if (!onGround(event) && prevVelChange.get(event.getPlayer().getUniqueId()) == Math.abs(event.getFrom().getY() - event.getTo().getY())) {
                    upwardMovement.put(event.getPlayer().getUniqueId(), upwardMovement.get(event.getPlayer().getUniqueId()) + 1);
                } else {
                    upwardMovement.put(event.getPlayer().getUniqueId(), 0);
                }
            }
        } else {
            upwardMovement.put(event.getPlayer().getUniqueId(), 0);
        }

        if (onGround(event)) {
            if (event.getTo().getY() - event.getFrom().getY() > 0.6) {
                return new Flag(50, "fly high jump check");
            }
        } else {
            if (upwardMovement.get(event.getPlayer().getUniqueId()) > 5 && prevVelChange.get(event.getPlayer().getUniqueId()) == Math.abs(event.getFrom().getY() - event.getTo().getY())) {
                return new Flag(50, "fly motion consistency check");
            } else if (upwardMovement.get(event.getPlayer().getUniqueId()) > 100) {
                return new Flag(50, "fly upward motion check");
            } else if (Math.abs(event.getFrom().getY() - event.getTo().getY()) < 0.01 && fromFlat.distanceSquared(toFlat) >= 1) {
                return new Flag(100, "fly moving hover check");
            }
        }

        if (prevVelChange.containsKey(event.getPlayer().getUniqueId())) {
            prevVelChange.put(event.getPlayer().getUniqueId(), Math.abs(event.getFrom().getY() - event.getTo().getY()));
        } else {
            prevVelChange.put(event.getPlayer().getUniqueId(), 0.0);
        }

        return new Flag();
    }

    private boolean onGround(PlayerMoveEvent event) {
        return ((Entity)event.getPlayer()).isOnGround();
    }

    @Override
    public void runTick(Player suspect) {}
}
