package top.cavecraft.cclib.skynet;

public class Flag {
    public final int suspicion;
    public final String flagReason;

    public Flag(int suspicion, String flagReason) {
        this.suspicion = suspicion;
        this.flagReason = flagReason;
    }

    public Flag() {
        suspicion = 0;
        flagReason = null;
    }
}
