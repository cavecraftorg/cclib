package top.cavecraft.cclib.skynet;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;

public abstract class Check {
    Class<? extends Event> type;

    public Check(Class<? extends Event> type) {
        this.type = type;
    }

    /**
     * Runs a check on a certain player.
     *
     * @param suspect
     * The player to check.
     *
     * @param event
     * The event that triggered this check.
     *
     * @return
     * The suspicion to add to this player. (Other anticheats call this "Violation Level")
     */
    public abstract Flag runAbstractCheck(Player suspect, Event event);

    /**
     * Runs every second on every player, so that you can, for example, check speed.
     * Note: this is run asynchronously, however that should not be an issue since the anticheat
     * won't be placing blocks.
     *
     * @param suspect
     * The player to check.
     */
    public abstract void runTick(Player suspect);

    public Class<? extends Event> getEventType() {
        return type;
    }
}
