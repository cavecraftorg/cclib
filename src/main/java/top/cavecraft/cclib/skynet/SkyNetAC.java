package top.cavecraft.cclib.skynet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.*;

import static top.cavecraft.cclib.CCLib.plugin;

public class SkyNetAC implements Listener, CommandExecutor {
    public static SkyNetAC anticheat;
    private final List<Check> checks = new ArrayList<>();
    private final int maxSuspicion = 500;
    private final Map<UUID, Integer> suspicionMap = new HashMap<>();

    String banMessage = "" + ChatColor.RED + ChatColor.BOLD + "YOU ARE BANNED FROM THIS SERVER.\n" + ChatColor.WHITE + "          You were banned by " + ChatColor.BLUE + "SKYNET AntiCheat\n" + ChatColor.WHITE + "for hacking or glitch abuse.";
    //This is 30 days in milliseconds.
    long banTime = 2592000000L;

    public SkyNetAC() {
        anticheat = this;

        plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, this::runTick, 20, 20);
        plugin.getServer().getPluginCommand("skynet").setExecutor(this);
        plugin.getServer().getPluginCommand("sk").setExecutor(this);
        plugin.getServer().getPluginCommand("skynetac").setExecutor(this);
        plugin.getServer().getPluginCommand("skac").setExecutor(this);
    }

    public void runTick() {
        for (Check check : checks) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                check.runTick(player);
                if (suspicionMap.getOrDefault(player.getUniqueId(), 0) > 0) {
                    suspicionMap.put(player.getUniqueId(), suspicionMap.getOrDefault(player.getUniqueId(), 0) - 25);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void join(PlayerJoinEvent event) {
        checkBan(event.getPlayer());
    }

    public void banKickPlayer(Player player) {
        Date endDate = new Date(System.currentTimeMillis() + banTime);
        plugin.players.get(player.getUniqueId().toString())
                .setValue("banned", true)
                .setValue("endDate", endDate)
                .setValue("name", player.getName());
        player.kickPlayer(ChatColor.RED + "Sorry, hacker. This server is protected by" + ChatColor.BLUE + " SKYNET" + ChatColor.WHITE + ". You have been banned for 30 days.");
        suspicionMap.put(player.getUniqueId(), 0);
    }

    public void checkBan(Player player) {
        plugin.players.get(player.getUniqueId().toString())
                .setValue("name", player.getName());

        if ((Boolean) plugin.players.get(player.getUniqueId().toString()).getValueOrDefault("banned", false)) {
            Date endDate = (Date) plugin.players.get(player.getUniqueId().toString()).getValue("endDate");
            if (endDate.after(new Date())) {
                player.kickPlayer(banMessage + "\n Your ban ends: " + endDate.toString());
            } else {
                player.sendMessage(ChatColor.RED + "You have been banned before. Keep in mind that " + ChatColor.ITALIC + "SKYNET is always watching.");
            }
        }
    }

    public void addSuspicion(Flag flag, Player player) {
        suspicionMap.put(player.getUniqueId(), suspicionMap.getOrDefault(player.getUniqueId(), 0) + flag.suspicion);

        if (flag.suspicion > 0) {
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (p.hasPermission("cclib.moderator")) {
                    p.sendMessage(ChatColor.RED + "SKYNET" + ChatColor.WHITE + " -> " + player.getName() + " triggered " + flag.flagReason + ".");
                    p.sendMessage(ChatColor.RED + "Suspicion: " + ChatColor.WHITE + (suspicionMap.get(player.getUniqueId()) + flag.suspicion));
                }
            }
            plugin.getLogger().warning(ChatColor.RED + "SKYNET" + ChatColor.WHITE + " -> " + player.getName() + " triggered " + flag.flagReason + ".");
            plugin.getLogger().warning(ChatColor.RED + "Suspicion: " + ChatColor.WHITE + (suspicionMap.get(player.getUniqueId()) + flag.suspicion));
        }

        if ((suspicionMap.get(player.getUniqueId()) + flag.suspicion) > maxSuspicion) {
            banKickPlayer(player);
        }
    }

//    @EventHandler
//    public void kick(PlayerKickEvent event) {
//        if (event.getReason().equals("Flying is not enabled on this server")) {
//            event.setCancelled(true);
//            addSuspicion(new Flag(50, "vanilla fly kick"), event.getPlayer());
//            Location floor = event.getPlayer().getLocation();
//            while (floor.getBlock().getType() == Material.AIR && floor.getY() > 0) {
//                floor.add(0, -1, 0);
//            }
//            floor.add(0, 1, 0);
//            event.getPlayer().teleport(floor);
//            event.getPlayer().setFlying(false);
//        }
//    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("skynet") || label.equalsIgnoreCase("sk") || label.equalsIgnoreCase("skac") || label.equalsIgnoreCase("skynetac")) {
            if (!sender.hasPermission("cclib.moderator")) return false;
            if (args[0] == null) {
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/sk cs <player>");
                sender.sendMessage("Make sure your player is valid.");
                return true;
            }
            switch (args[0]) {
                case "addsuspicion":
                case "as":
                    if (!sender.isOp()) return false;
                    if (Bukkit.getPlayer(args[1]) != null && args.length == 4 && !args[1].startsWith("*")) {
                        try {
                            //addSuspicion(new Flag(Integer.parseInt(args[2]), args[3]), Bukkit.getPlayer(args[1]));
                            sender.sendMessage("Suspicion increased by " + Integer.parseInt(args[2]));
                        } catch (Exception e) {
                            sender.sendMessage("That suspicion is not a number.");
                        }
                    }
                    return true;
                case "checksuspicion":
                case "cs":
                    if (Bukkit.getPlayer(args[1]) != null && args.length == 2) {
                        sender.sendMessage(ChatColor.RED + "SKYNET" + ChatColor.WHITE + " -> " + "That player has a suspicion of " + suspicionMap.getOrDefault(Bukkit.getPlayer(args[1]).getUniqueId(), 0));
                    } else {
                        sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/sk cs <player>");
                        sender.sendMessage("Make sure your player is valid.");
                    }
                    return true;
                case "ban":
                case "b":
                    plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
                        if (Bukkit.getPlayer(args[1]) != null && args.length == 2) {
                            banKickPlayer(Bukkit.getPlayer(args[1]));
                            sender.sendMessage(ChatColor.RED + "SKYNET" + ChatColor.WHITE + " -> " + "That player has been banned from the network.");
                        } else {
                            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/sk b <player>");
                            sender.sendMessage("Make sure your player is valid.");
                        }
                    });
                    return true;
                case "unban":
                case "ub":
                    plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
                        if (!plugin.players.has("name", args[1]) && args.length == 2) {
                                plugin.players.get("name", args[1]).setValue("endDate", new Date());
                                sender.sendMessage(ChatColor.RED + "SKYNET" + ChatColor.WHITE + " -> " + "That player has been unbanned.");
                        } else {
                            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.WHITE + "/sk ub <player>");
                            sender.sendMessage("Make sure your player is valid.");
                        }
                    });
                    return true;
                default:
                    sender.sendMessage(ChatColor.RED + "SKYNET" + ChatColor.WHITE + " -> " + "Skynet is a semi-anticheat that combines abilities of NCP and ACR along with some extra checks and code.");
                    sender.sendMessage(ChatColor.WHITE + "The base commands are as follows:");
                    sender.sendMessage(ChatColor.WHITE + "/skynet checksuspicion <player> (/sk cs <player>): Get the suspicion of a player. Players are banned after " + maxSuspicion + " suspicion.");
                    sender.sendMessage(ChatColor.WHITE + "/skynet ban <player> (/sk b <player>): Bans a player instantly.");
                    sender.sendMessage(ChatColor.WHITE + "/skynet unban <player> (/sk ub <player>): Unbans a player, only use if you are sure they were falsely banned.");
                    sender.sendMessage(ChatColor.WHITE + "There are other commands, but they are for use by plugins and developers only.");
            }
        }
        return false;
    }
}